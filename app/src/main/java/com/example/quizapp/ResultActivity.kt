package com.example.quizapp

import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.io.File
import java.io.FileWriter
import java.util.*
import javax.mail.*
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart


class ResultActivity : AppCompatActivity() {
    private var tv_name: TextView? = null
    private var tv_score: TextView? = null
    private var btn_finish: Button? = null
    private var btn_share: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        tv_name = findViewById(R.id.tv_name)
        tv_score = findViewById(R.id.tv_score)
        btn_finish = findViewById(R.id.btn_finish)
        btn_share = findViewById(R.id.btn_share)

        val username = intent.getStringExtra(Constants.USER_NAME)
        tv_name!!.text = username
        val totalQuestions = intent.getIntExtra(Constants.TOTAL_QUESTIONS, 0)
        val correctAnswears = intent.getIntExtra(Constants.CORRECT_ANSWEARS, 0)

        tv_score!!.text = "Your score is $correctAnswears out of $totalQuestions"

        btn_finish!!.setOnClickListener(){
            val textM: String = "My new log"
            var file = File(this.getFilesDir(),"text")
            var gpxfile:File?= null

            if (!file.exists()) {
                file.mkdir();
            }
            try {
                gpxfile = File(file, "sample")
                val writer = FileWriter(gpxfile)
                writer.append(textM)
                writer.flush()
                writer.close()
                //Toast.makeText(this, "Saved your text", Toast.LENGTH_LONG).show();
            }catch (e:Exception){
                throw RuntimeException(e)
            }

            //start of log sending
            val text:String = "File with log is below"

            val userName: String = "ilchuk.se@gmail.com"
            val password: String = "tolik0613"
            val messageToSend: String = text
            val props: Properties = Properties()
            props.put("mail.smtp.auth","true")
            props.put("mail.smtp.starttls.enable","true")
            props.put("mail.smtp.host","smtp.gmail.com")
            props.put("mail.smtp.port","587")

            val session: Session = Session.getInstance(props,
                    object: javax.mail.Authenticator(){
                        override fun getPasswordAuthentication(): PasswordAuthentication {
                            return PasswordAuthentication(userName, password)
                        }
                    }
            )
            try {
                val message: Message = MimeMessage(session)
                message.setFrom(InternetAddress(userName))
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("ilchuk.se@gmail.com "))
                message.setSubject("Sending new email log")
                message.setText(text)

                var attachmentPart: MimeBodyPart = MimeBodyPart()
                attachmentPart.attachFile(gpxfile)

                val multipart: Multipart = MimeMultipart()
                multipart.addBodyPart(attachmentPart)
                message.setContent(multipart);

                Transport.send(message)
                Toast.makeText(this, "log successfully send", Toast.LENGTH_LONG).show()
            }catch (e: MessagingException){
                throw RuntimeException(e)
            }
            //end of log sending

            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }

        btn_share!!.setOnClickListener(){
            var shareIntent = Intent().apply {
                this.action = Intent.ACTION_SEND
                this.putExtra(Intent.EXTRA_TEXT, "My score is $correctAnswears out of $totalQuestions in QuizApp. Can you do better?")
                this.type = "text/plain"
            }

            startActivity(shareIntent)
        }
        val policy: StrictMode.ThreadPolicy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
    }
}