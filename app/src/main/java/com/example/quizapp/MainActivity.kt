package com.example.quizapp

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.widget.AppCompatEditText

class MainActivity : AppCompatActivity() {

    private var btn_start: Button? = null
    private var et_name: AppCompatEditText? = null

    lateinit var notificationManager: NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var notificationBuilder: Notification.Builder
    private val channelId = "quizapp"
    private val description = "test notification"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_start = findViewById(R.id.btn_start);
        et_name = findViewById(R.id.et_name);

        btn_start?.setOnClickListener() {
            generateNotification()
            if(et_name?.text.toString().isEmpty()) {
                Toast.makeText(this, "Please enter your name", Toast.LENGTH_SHORT).show()
            } else{
                val intent = Intent(this, QuizQuestionsActivity::class.java)
                intent.putExtra(Constants.USER_NAME, et_name?.text.toString())
                startActivity(intent)
                finish()
            }
        }
    }

    private fun generateNotification() {
        val notificationIntent: Intent = Intent(this, MainActivity::class.java)
        val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT)

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = NotificationChannel(channelId, description, NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.GREEN
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)

            notificationBuilder = Notification.Builder(this, channelId)
                    .setContentTitle("QuizApp")
                    .setContentText("Welcome back")
                    .setSmallIcon(R.drawable.ic_bg)
                    .setChannelId(channelId)
                    .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.ic_flag_of_ukraine))
                    .setContentIntent(pendingIntent)
        }else{
            notificationBuilder = Notification.Builder(this)
                    .setContentTitle("QuizApp")
                    .setContentText("Welcome back")
                    .setSmallIcon(R.drawable.ic_bg)
                    .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.ic_flag_of_ukraine))
                    .setContentIntent(pendingIntent)
        }

        notificationManager.notify(1234, notificationBuilder.build())
    }
}