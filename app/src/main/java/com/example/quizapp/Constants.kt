package com.example.quizapp

object Constants {

    const val USER_NAME: String = "user_name"
    const val TOTAL_QUESTIONS: String = "total_questions"
    const val CORRECT_ANSWEARS: String = "correct_answers"

    fun getQuestions() : ArrayList<Question>{
        val questionsList = ArrayList<Question>()

        val que1 = Question(1,
            "Who invented the automobile?",
            R.drawable.ic_first_car ,
            "Carl Benz", "Henry Ford",
            "Ferdinand Porsche", "Gottlieb Daimler", 1
        )
        questionsList.add(que1)

        val que2 = Question(2,
            "Which car company is the most expensive",
            R.drawable.ic_auto,
            "Ferrari", "Volkswagen",
            "Ford", "Toyota", 2
        )
        questionsList.add(que2)

        val que3 = Question(3,
            "What country does this flag belong to?",
            R.drawable.ic_flag_of_crotia ,
            "Argentina", "Ukraine",
            "Crotia", "Poland", 3
        )
        questionsList.add(que3)

        val que4 = Question(4,
            "What country does this flag belong to?",
            R.drawable.ic_flag_of_czech ,
            "Argentina", "Ukraine",
            "Bolivia", "Czech", 4
        )
        questionsList.add(que4)

        val que5 = Question(5,
            "What is the highest-grossing film?",
            R.drawable.ic_film,
            "Star wars", "Gone with the Wind",
            "Titanic", "Avatar", 2
        )
        questionsList.add(que5)

        val que6 = Question(6,
            "What is the highest lifetime-grossing film?",
            R.drawable.ic_film2 ,
            "Avatar", "Titanic",
            "Avangers:Endgame", "Star Wars: Episode 6", 1
        )
        questionsList.add(que6)

        val que7 = Question(7,
            "Which pc OS is the most popular?",
            R.drawable.ic_os,
            "Windows", "Linux",
            "Mac OS", "DOS", 1
        )
        questionsList.add(que7)

        val que8 = Question(8,
            "Which OS is the most popular?",
            R.drawable.ic_os2 ,
            "Windows", "Linux",
            "IOS", "Android", 4
        )
        questionsList.add(que8)

        val que9 = Question(9,
            "What country does this flag belong to?",
            R.drawable.ic_flag_of_usa ,
            "USA", "Ukraine",
            "Bolivia", "Czech", 1
        )
        questionsList.add(que9)

        val que10 = Question(10,
            "What country does this flag belong to?",
            R.drawable.ic_flag_of_bolivia,
            "Argentina", "Ukraine",
            "Bolivia", "Czech", 3
        )
        questionsList.add(que10)

        return questionsList
    }

}